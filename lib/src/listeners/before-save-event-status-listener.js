import { DataObjectState } from "@themost/data";

/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
  if (event.state === DataObjectState.Insert || event.state === DataObjectState.Update) {

    /**
     * @type {import('@themost/express').ExpressDataContext}
     */
    const context = event.model.context;
    

    if (event.target.eventStatus != null) {
      const eventStatus = context.model('EventStatusType').convert(event.target.eventStatus);
      if (eventStatus != null && eventStatus.alternateName === 'EventScheduled') {

        const eventStatus = context.model('EventStatusType').convert(event.target.eventStatus);
        if (eventStatus != null && eventStatus.alternateName === 'EventScheduled') {

          if(event.state === DataObjectState.Update) {
            if(!event.target.hasOwnProperty('startDate') || !event.target.hasOwnProperty('endDate')) {
            // load dates
              const eventObject = await context.model('Event')
                .where('id').equal(event.target.id).getItem();
              if (eventObject) {
                event.target.startDate = !event.target.hasOwnProperty('startDate') ? eventObject.startDate : event.target.startDate;
                event.target.endDate = !event.target.hasOwnProperty('endDate') ? eventObject.endDate : event.target.endDate;
              }
            }
          }
        if ((!event.target.hasOwnProperty('startDate') || event.target.startDate == null)
          || (!event.target.hasOwnProperty('endDate') || event.target.endDate == null)) {
          throw new Error('Start date and end date are required for scheduled events');;
        }
      }
    }
  }
}
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
  return beforeSaveAsync(event).then(() => {
    return callback();
  }).catch((err) => {
    return callback(err);
  });
}

