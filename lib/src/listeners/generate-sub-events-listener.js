import {parseExpression} from 'cron-parser';
import moment from 'moment';
const Event = require('../models/event-model');

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return callback();
}

/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    if (event.state === 1) {
        if (!event.target.hasOwnProperty('eventHoursSpecification') || event.target.eventHoursSpecification === null) {
            return;
        }
        const context = event.model.context;

        //get target event hours specification
        let thisEventHoursSpecification = typeof event.target.eventHoursSpecification !== 'object' ?
            await context.model('EventHoursSpecification')
                .where('id').equal(event.target.eventHoursSpecification)
                .getTypedItem() :
            context.model('EventHoursSpecification').convert(event.target.eventHoursSpecification);
        //fetch original data object
        const eventHoursSpecification = await context.model('EventHoursSpecification').where('id').equal(thisEventHoursSpecification.getId())
            .silent().getTypedItem();
        if (eventHoursSpecification) {
            //check if validFrom and validThrough are defined
            if (eventHoursSpecification.validFrom == null || eventHoursSpecification.validThrough == null) {
                return;
            }
            //generate events
            let cronjob = eventHoursSpecification.toCronJobString();
            //get intervals
            let options = {
                // When currentDate matches given cron expression, cron-parser will miss current event
                // Subtract one second from the currentDate to avoid this
                // https://github.com/harrisiirak/cron-parser/issues/289
                currentDate: new Date(new Date(eventHoursSpecification.validFrom) - 1000),
                endDate: eventHoursSpecification.validThrough,
                iterator: true
            };
            let interval = parseExpression(cronjob, options);
            let intervals = [];
            while (interval) {
                try {
                    let obj = interval.next();
                    intervals.push(obj.value);
                } catch (err) {
                    break;
                }
            }

            let exceptDates;

            // eslint-disable-next-line no-unused-vars
            const { id, dateCreated, dateModified, ..._event} = event.target;
            const timetable = await context.model('TimetableEvent').where('id').equal(_event.superEvent).getItem();
            if (timetable) {
                exceptDates = await context.model('ExceptHoursSpecification').where('academicYear').equal(timetable.academicYear).expand('eventExceptStatus').getAllItems();
            }

            const events = []
            for (const interval of intervals) {
                const event = {
                    ..._event,
                    startDate: interval.toDate(),
                    endDate: moment(interval.toDate()).add(moment.duration(eventHoursSpecification.duration)).toDate(),
                    duration: eventHoursSpecification.duration,
                    superEvent: id,
                    eventHoursSpecification: null // sub-events shouldn't have eventHoursSpecification
                }

                if (exceptDates) {
                    const exceptDate = Event.getExceptDate(event.startDate, event.endDate, exceptDates);

                    // don't create sub-events for except dates according to the timetable's settings
                    if (exceptDate && !timetable.createExceptEvents) {
                        continue;
                    }

                    // create sub-events with specific status according to the except date's settings
                    if (exceptDate && exceptDate.updateEventStatus) {
                        event.eventStatus = exceptDate.eventExceptStatus;
                    }
                }

                events.push(event);
            }

            await event.model.context.model(event.target.additionalType).silent().save(events);
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    // execute async method
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch(err => {
        return callback(err);
    });
}
