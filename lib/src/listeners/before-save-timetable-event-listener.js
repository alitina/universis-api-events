import { DataError } from "@themost/common";
import { DataEventArgs } from "@themost/data";

function id(input) {
    if (!input) throw new Error('Input cannot be null');
    return typeof input === 'object' ? input.id : input;
}

function validateEventDates(event, timetableEvent) {
    const { target: { startDate, endDate }, previous: { startDate: prevStartDate, endDate: prevEndDate }, model: { context }} = event;

    // If the new startDate is le than the old one and the new endDate is ge than the old one,
    // the timetable's time-frame can safely be changed without further investigation (no events will fall out of it)
    if (startDate <= prevStartDate && endDate >= prevEndDate) {
        return;
    }

    const timetableEventStartDate = timetableEvent.eventHoursSpecification ? timetableEvent.eventHoursSpecification.validFrom : timetableEvent.startDate;
    const timetableEventEndDate = timetableEvent.eventHoursSpecification ? timetableEvent.eventHoursSpecification.validThrough : timetableEvent.endDate;

    if (startDate >= timetableEventStartDate || new Date(endDate).setHours(23, 59, 59, 59) <= timetableEventEndDate) {
        throw new Error(`Cannot change the timetable's time-frame. There are events scheduled prior to ${startDate.toLocaleDateString(context.locale)} or after ${endDate.toLocaleDateString(context.locale) }`);
    }
}


async function validateEvents(event, timetableEvents = [], timetableJunctions = []) {
    const { target, previous, model: { context } } = event;

    for (const timetableEvent of timetableEvents) {
        const typedEvent = await context.model(timetableEvent.additionalType).convert(timetableEvent);

        validateEventDates(event, timetableEvent)

        if (id(target.academicYear) !== id(previous.academicYear) && 'getYear' in typedEvent) {
            const year = await typedEvent.getYear();
            if (year.id !== id(target.academicYear)) {
                throw new Error(`Cannot change academic year. There are events associated with the year "${year.name}".`);
            }
        }

        const removedAcademicPeriods = timetableJunctions.find(junction => junction.name === 'academicPeriods' && junction.removed)
        if (removedAcademicPeriods && 'getPeriod' in typedEvent) {
            const period = await typedEvent.getPeriod();
            if (!target.academicPeriods.find(academicPeriod => id(academicPeriod) === period.id )) {
                throw new Error(`Cannot remove academic period. There are events associated with the period "${period.name}"`);
            }
        }

        // TODO: test this when examEvents are implemented
        const removedExamPeriods = timetableJunctions.find(junction => junction.name === 'examPeriods' && junction.removed)
        if (removedExamPeriods && 'getExamPeriod' in typedEvent) {
            const period = await typedEvent.getExamPeriod();
            if (!target.examPeriods.find(examPeriod => id(examPeriod) === period.id)) {
                throw new Error(`Cannot remove exam period. There are events associated with the period "${period.name}"`);
            }
        }

        const removedAvailablePlaces = timetableJunctions.find(junction => junction.name === 'availablePlaces' && junction.removed)
        if (timetableEvent.location && target.availablePlaces && removedAvailablePlaces) {
            if (!target.availablePlaces.find(place => id(place) === timetableEvent.location.id)) {
                throw new Error(`Cannot remove place. There are events associated with the place "${timetableEvent.location.name}"`);
            }
        }

        const removedEventTypes = timetableJunctions.find(junction => junction.name === 'availableEventTypes' && junction.removed)
        if (target.availableEventTypes && removedEventTypes) {
            const eventType = await context.model('EventType').where('alternateName').equal(timetableEvent.additionalType).getItem();
            if (!target.availableEventTypes.find(type => id(type) === eventType.id)) {
                throw new Error(`Cannot remove event type. There are events of type "${eventType.name}" registered in the timetable`);
            }
        }
    }
}

async function processJunctions(event) {
    const target = event.model.convert(event.target);
    const junctions = event.model.fields.filter(field => field.mapping && field.mapping.associationType === 'junction').map(field => ({
        name: field.name,
        removed: false
    }));

    for (const junction of junctions) {
        const current = target[junction.name];
        if (current && Array.isArray(current)) {
            const objectJunction = await target.property(junction.name);
            const previous = await objectJunction.getAllItems();
            if (Array.isArray(previous) && previous.length) {
                // if an empty array is submitted remove all objects
                if (current.length === 0) {
                    await objectJunction.removeAll();
                    junction.removed = true;
                    continue;
                }

                // else remove the ones not present in the submitted array
                for (const prev of previous) {
                    if (!current.find(curr => id(curr) === id(prev))) {
                        await objectJunction.remove(prev);
                        junction.removed = true;
                    }
                }
            }
        }
    }
    return junctions;
}

/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    if (event.state === 2) {

        if (event.previous == null) {
            throw new DataError('E_STATE', 'Previous state cannot be empty while updating.', null, 'Timetable');
        }

        if (!event.target.academicYear) {
            throw new Error('Academic year cannot be null');
        }

        const timetableEvents = await event.model.context.model('Events')
            .where('superEvent')
            .equal(event.target.id)
            .expand('eventHoursSpecification', 'location')
            .take(-1)
            .getItems();

        const junctions = await processJunctions(event);
        await validateEvents(event, timetableEvents, junctions)

    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
