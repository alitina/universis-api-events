import { DataError } from "@themost/common";
import { DataEventArgs } from "@themost/data";

/**
 * @param {DataEventArgs} event
 */
async function beforeSaveAsync(event) {
    if (event.state <= 2) {
        const { target: { eventExceptStatus, updateEventStatus, validFrom, validThrough, academicYear }, model: {context} } = event;

        if (updateEventStatus) {
            if (!eventExceptStatus) throw new Error('EventExceptStatus cannot be null if updateEventStatus is set to true');

            const newStatus = await context.model('EventStatusType').where('id').equal(eventExceptStatus).getItem();

            const timetables = await context.model('TimetableEvent')
                .where('academicYear').equal(academicYear)
                .getAllItems();

            for (const timetable of timetables) {
                const timetableEvents = await context.model('Events')
                    .where('superEvent').equal(timetable.id)
                    .and('eventHoursSpecification').equal(null)
                    .or('superEvent/superEvent').equal(timetable.id)
                    .prepare()
                    .and('startDate').greaterOrEqual(validFrom)
                    .and('endDate').lowerOrEqual(validThrough)
                    .and('dateCreated').greaterOrEqual(new Date(new Date().setHours(0, 0, 0, 0)))
                    .getAllItems()

                for (const event of timetableEvents) {
                    event.eventStatus = newStatus;
                    await context.model('Events').save(event);
                }
            }
        }
    }
}

/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function beforeSave(event, callback) {
    return beforeSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
