import {DataObject} from "@themost/data";

declare class GeoCoordinates extends DataObject {}

export = GeoCoordinates;
