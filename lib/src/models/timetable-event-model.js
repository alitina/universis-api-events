import { EdmMapping, EdmType } from '@themost/data';
import { DataError, Guid } from "@themost/common";
import { ValidationResult } from '../errors';
import moment from 'moment';
const ejs = require('ejs');
const Event = require('./event-model');

/**
 * @class
 * @property {Array<Place>} availablePlaces
 */
@EdmMapping.entityType('TimetableEvent')
class TimetableEvent extends Event {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    @EdmMapping.param('data', 'Object', false, true)
    @EdmMapping.action('copy', 'CopyTimetableEventAction')
    async copy(data) {
        if (!data.academicYear || !data.startDate) {
            throw new DataError('E_INVALID_DATA', 'Missing required parameters. Either academic year or start date is missing.', null, 'TimetableEvent');
        }

        const action = await this.context.model('CopyTimetableEventAction').save({ timetableEvent: this.getId() });
        this.copyTimetable(action, data);
        return action;
    }

    async copyTimetable(action, data) {
        try {
            const timetable = await this.getModel()
                .where('id').equal(this.getId())
                .expand('availablePlaces', 'availableEventTypes', 'academicPeriods', 'examPeriods')
                .getItem();

            const timetableEvents = await this.context.model('Event')
                .where('superEvent')
                .equal(timetable.id)
                .expand('eventHoursSpecification')
                .take(-1)
                .getItems();


            // save new timetable
            // eslint-disable-next-line no-unused-vars
            const { id, dateModified, dateCreated, ...timetableBase } = timetable;
            const newTimetable = await this.getModel().save({
                ...Event.calculateDatesByDate(timetableBase, data.startDate),
                academicYear: typeof data.academicYear === 'object' ? data.academicYear.id : data.academicYear,
                description: data.description || null,
                name: data.name || `${timetable.name} - copy`,
                eventStatus: { alternateName: 'EventOpened' },
                eventHoursSpecification: null,
                createExceptEvents: data.hasOwnProperty('createExceptEvents') ? data.createExceptEvents : timetableBase.createExceptEvents
            });

            // get except dates that refer to the new timetable's academic year
            const exceptDates = await this.context.model('ExceptHoursSpecification')
                .where('academicYear').equal(newTimetable.academicYear)
                .expand('eventExceptStatus')
                .getAllItems();

            const failures = [];
            for (const event of timetableEvents) {
                const getTimeFrame = ({ startDate, endDate }) => ({ startDate, endDate })
                const updatedEvent = Event.calculateDatesByTimeFrame({ ...event, superEvent: newTimetable.id }, getTimeFrame(newTimetable), getTimeFrame(timetable));
                try {
                    const savedEvent = await this.context.model(event.additionalType).convert(event).addToTimetable(updatedEvent, newTimetable);
                    // except dates are applied to recurring events via the sub-events listener
                    // apply exceptions to individual events as well 
                    if (!savedEvent.eventHoursSpecification) {
                        try {
                            await this.applyExceptDate(savedEvent, newTimetable.createExceptEvents, exceptDates)
                        } catch (error) {
                            // pass
                        }
                    }
                } catch (error) {
                    failures.push({event, error})
                }
            }

            // failures template
            const template = `<% failures.forEach(function(failure){ -%>
                <% if(!failure.event.eventHoursSpecification){ -%>
                    <p>The event with title <strong><%- failure.event.name %></strong>, start date <strong><%- new Date(failure.event.startDate).toLocaleDateString('${this.context.locale}') %></strong>, end date <strong><%- new Date(failure.event.endDate).toLocaleDateString('${this.context.locale}') %></strong> could not be copied.<br/><strong>Reason</strong>: <%- failure.error.message -%></p>
                <% } else { -%>
                    <p>The recurring event with title <strong><%- failure.event.name %></strong>, which was valid from <strong><%- new Date(failure.event.eventHoursSpecification.validFrom).toLocaleDateString('${this.context.locale}') %></strong> through <strong><%- new Date(failure.event.eventHoursSpecification.validThrough).toLocaleDateString('${this.context.locale}') %></strong> and its sub-events could not be copied.<br/><strong>Reason</strong>: <%- failure.error.message -%></p>
                <% } -%>
            <% }); -%>`

            // after the timetable is copied, update actionStatus
            action.actionStatus = { alternateName: 'CompletedActionStatus' };
            action.endTime = new Date();
            action.result = newTimetable;
            action.failures = failures.length ? ejs.render(template, { failures }, { rmWhitespace: true }) : null;
            this.context.model('CopyTimetableEventAction').save(action);
        } catch (error) {
            // in case of error, update actionStatus as failed
            action.actionStatus = { alternateName: 'FailedActionStatus' };
            action.endTime = new Date();
            this.context.model('CopyTimetableEventAction').save(action);
            throw error
        }
    }

    async applyExceptDate(event, createExceptEvents, exceptDates = []) {
        const exceptDate = Event.getExceptDate(event.startDate, event.endDate, exceptDates);

        // timetable settings apply first: delete the event if it coincides with an except date
        if (exceptDate && !createExceptEvents) {
            await this.context.model(event.additionalType).remove(event);

            const validationMessage = this.context.__('The except date was applied successfully.')
            const validationInnerMessage = this.context.__('The event was removed.')
            return Object.assign(new ValidationResult(true, 'SUCC', validationMessage, validationInnerMessage, { event, exceptDate }));
        }

        // update the event with a specific status according to the except date's settings
        if (exceptDate && exceptDate.updateEventStatus) {
            event.eventStatus = exceptDate.eventExceptStatus;
            await this.context.model(event.additionalType).save(event);

            const validationMessage = this.context.__('The except date was applied successfully.')
            const validationInnerMessage = this.context.__('The event\'s status changed.')
            return Object.assign(new ValidationResult(true, 'SUCC', validationMessage, validationInnerMessage, { event, exceptDate }));
        }
    }

    @EdmMapping.action('applyExceptDates', EdmType.CollectionOf('ValidationResults'))
    async applyExceptDates() {
        // get the timetable and all its events except recurring ones
        const timetable = await this.getModel().where('id').equal(this.getId()).getItem();
        const timetableEvents = await this.context.model('Event')
            .where('superEvent').equal(timetable.id)
            .and('eventHoursSpecification').equal(null)
            .or('superEvent/superEvent').equal(timetable.id)
            .prepare()
            .and('eventStatus/alternateName').equal('EventOpened')
            .getAllItems()

        const exceptDates = await this.context.model('ExceptHoursSpecification').where('academicYear').equal(timetable.academicYear).expand('eventExceptStatus').getAllItems();

        const validationResults = [];
        for (const event of timetableEvents) {
            try {
                const validationResult = await this.applyExceptDate(event, timetable.createExceptEvents, exceptDates)
                if (validationResult) {
                    validationResults.push(validationResult)
                }
            } catch (error) {
                const validationMessage = this.context.__('The except date could not be applied.')
                validationResults.push(Object.assign(new ValidationResult(false, 'FAIL', validationMessage, error.message, event)));
            }
        }
        return validationResults;
    }

    @EdmMapping.func("availablePlaces", EdmType.CollectionOf("Places"))
    async getPlaces() {
        return this.getModel().where('id').equal(this.getId()).expand('availablePlaces').getItem().then(timetable => timetable.availablePlaces);
    }

    @EdmMapping.param("data", "Object", false, true)
    @EdmMapping.action('semestersBySpecialization', 'Object')
    async semesters(data) {
        // TODO: get only those that span across the time-frame of the timetable
        const events = await this.context.model('TeachingEvents')
            .where('eventHoursSpecification').notEqual(null)
            .and('superEvent').equal(this.getId())
            .groupBy('courseClass/course')
            .select('courseClass/course as course')
            .getAllItems();

        const semestersBySpecialization = await this.context.model('SpecializationCourse')
            .where('studyProgramCourse/studyProgram').equal(data.studyProgram)
            .and('studyProgramCourse/course').in(events.map(event => `${event.course}`))
            .groupBy('semester', 'specialization', 'specialization/specialty')
            .select('semester', 'specialization')
            .expand('semester', 'specialization')
            .orderBy('semester').thenBy('specialization/specialty')
            .getAllItems();
        
        const timetable = await this.getModel().where('id').equal(this.getId()).expand('academicPeriods', 'academicYear', 'examPeriods', 'availableEventTypes').getItem();
        const organizer = await this.context.model('Departments').where('id').equal(timetable.organizer).getItem();

        return {
            ...timetable,
            semestersBySpecialization,
            organizer
        }
    }

    @EdmMapping.param("data", "Object", false, true)
    @EdmMapping.action('calculatePrintEvents', 'Object')
    async calculatePrintEvents(data) {
        // get all the teaching events of the timetable
        let events = await this.context.model('TeachingEvent')
            .where('eventHoursSpecification').notEqual(null)
            .and('superEvent').equal(this.getId())
            .select('id', 'courseClass/course as course', 'courseClass/title as classTitle', 'courseClass/id as class', 'courseClass/course/displayCode as courseCode', 'eventHoursSpecification/opens as opens', 'eventHoursSpecification/closes as closes', 'eventHoursSpecification/id as eventHoursSpecification', 'eventHoursSpecification/dayOfWeek as day', 'location/name as location', 'performer/alternateName as performer', 'location/displayCode as locationCode', 'eventStatus/alternateName as eventStatus')
            .groupBy('id', 'courseClass/course', 'courseClass/title', 'courseClass/id', 'courseClass/course/displayCode', 'eventHoursSpecification/opens', 'eventHoursSpecification/closes', 'eventHoursSpecification/id', 'eventHoursSpecification/dayOfWeek', 'location/name', 'performer/alternateName', 'location/displayCode', 'eventStatus/alternateName')
            .expand('sections')
            .orderBy('eventHoursSpecification/opens')
            .getAllItems()

        // get the courses corresponding to the events, for the given semester and specialization 
        let specializationCourses = await this.context.model('SpecializationCourse')
            .where('studyProgramCourse/studyProgram').equal(data.studyProgram)
            .and('studyProgramCourse/course').in(events.map(event => `${event.course}`))
            .and('semester').equal(data.semester)
            .and('specialization').equal(data.specialization)
            .select('studyProgramCourse/course as course')
            .getAllItems();
        specializationCourses = specializationCourses.map(course => course.course);

        // keep only the events that correspond to the courses of the given semester and specialization 
        events = events.filter(event => specializationCourses.includes(event.course))

        const eventsBySection = []
        const eventsByDay = [];
        const finalEvents = [];


        // group the events by their sections
        for (const event of events) {
            event.opens = event.opens.toTimeString();
            event.closes = event.closes.toTimeString();

            if (event.sections.length) {
                for (const section of event.sections) {
                    eventsBySection.push({
                        ...event,
                        section: section.name,
                        sectionId: section.id
                    })
                }
            } else {
                eventsBySection.push({
                    ...event,
                    section: null
                })
            }
        }

        // group the events by the days the occur
        for (const event of eventsBySection) {
            if (event.day === '*') { event.day = '0,1,2,3,4,5,6' }

            if (event.day) {
                for (const day of event.day.split(',')) {
                    eventsByDay.push({
                        ...event,
                        day: TimetableEvent.dayIndexToDay(day, this.context.locale),
                        dayId: parseInt(day)
                    })
                }
            }
        }

        /* group events like this:
        {
            classID: [[], [monEv1, monEv2], [], [], [thuEv1, thuEv2], [friEv1], []]
        }
        */
        const courseClassEvents = {}
        for (const event of eventsByDay) {
            if (!courseClassEvents[event.class]) {
                courseClassEvents[event.class] = Array.from({ length: 7 }, () => []);
            }

            courseClassEvents[event.class][parseInt(event.dayId)].push(event)
        }

        for (const classEvents of Object.values(courseClassEvents)) {

            // sort events within each day
            for (const dayEvents of classEvents) {
                dayEvents.sort((a, b) => a.opens > b.opens ? 1 : -1);
            }

            // determine maximum number of events on a single day
            const maxEvents = Math.max(...classEvents.map(dayEvents => dayEvents.length))

            // assign the same id to events that should appear in the same row in the exported calendar
            // e.g monEv1, thuEv1, friEv1 should have the same id in order to appear in the same row
            for (let eventIndex = 0; eventIndex < maxEvents; eventIndex++) {
                const guid = Guid.newGuid().toString()

                for (let dayIndex = 0; dayIndex < classEvents.length; dayIndex++) {
                    if (eventIndex < classEvents[dayIndex].length) {
                        const dayEvent = classEvents[dayIndex][eventIndex]
                        dayEvent.id = guid
                        finalEvents.push(dayEvent)
                    }
                }
            }
        }

        return finalEvents;
    }

    static dayIndexToDay(dayIndex, locale) {
        return moment().day(dayIndex).locale(locale).format('dddd');
    }

}

module.exports = TimetableEvent;
