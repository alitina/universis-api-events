import {EdmMapping, DataObject} from '@themost/data';

/**
 * @class
 
 * @property {number} id
 * @augments {DataObject}
 */
@EdmMapping.entityType('EventStatusType')
class EventStatusType extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = EventStatusType;
