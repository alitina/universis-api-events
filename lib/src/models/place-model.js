import {EdmMapping, EdmType, DataObject} from '@themost/data';

/**
 * @class
 
 * @property {Array<Event|any>} events
 * @property {string} globalLocationNumber
 * @property {number} maximumAttendeeCapacity
 * @property {string} map
 * @property {string} branchCode
 * @property {PostalAddress|any} address
 * @property {string} logo
 * @property {string} telephone
 * @property {GeoCoordinates|any} geo
 * @property {Place} containedInPlace
 * @property {boolean} publicAccess
 * @property {string} faxNumber
 * @property {boolean} isAccessibleForFree
 * @augments {DataObject}
 */
@EdmMapping.entityType('Place')
class Place extends DataObject {
    /**
     * @constructor
     */
    constructor() {
        super();
    }

    /**
    * Static method that adds a descriptive property to each place.
    * This property provides information about the tree structure of the places.
    */
    @EdmMapping.func("flattened", EdmType.CollectionOf('Places'))
    static async getFlattenedPlaces(context, places) {
        //  get all available places
        const sourcePlaces = await context.model('Place').take(-1).getItems();
        // if no places-to-flatten are provided, flatten all available places
        if (!places) places = sourcePlaces;

        return context.model('Place').on('after.execute', (event, callback) => {
            (async () => {
                try {
                    for (const place of event.result) {
                        // add fullName property to each place
                        place.fullName = await Place.getFullName(place, sourcePlaces, null)
                    }
                } catch (error) {
                    callback(error);
                }
            })().then(callback)
        }).asQueryable();
    }


    /**
     * Static method to get a descriptive full name of a place, including its parents' names.
     * e.g. Apartment 12 - Floor 1 - Building A
     */
    static async getFullName(target, places, context) {
        // TODO: ensure that target has containedInPlace properties
        //  target has no parents, return its name 
        if (!target.containedInPlace) return target.name;

        // return target's name prepended with its parent's full name 
        const parentPlace = context
            ? await context.model('Places').where('id').equal(target.containedInPlace).getItem()
            : places.find(place => place.id === target.containedInPlace)
        if (parentPlace) return (target.name + ' - ' + await Place.getFullName(parentPlace, places, context))

        // if no parent place is found (but exists), throw an error
        throw new Error('The place has a parent but it could not be found in the provided source places.');
    }
}
module.exports = Place;
