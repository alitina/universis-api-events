// eslint-disable-next-line no-unused-vars
import {EdmMapping, DataObject} from '@themost/data';
const ContactPoint = require('./contact-point-model');
/**
 * @class
 
 * @property {string} postOfficeBoxNumber
 * @property {string} streetAddress
 * @property {string} addressCountry
 * @property {string} addressRegion
 * @property {string} postalCode
 * @property {string} addressLocality
 * @augments {DataObject}
 */
@EdmMapping.entityType('PostalAddress')
class PostalAddress extends ContactPoint {
    /**
     * @constructor
     */
    constructor() {
        super();
    }
}
module.exports = PostalAddress;
