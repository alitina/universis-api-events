import {ApplicationService} from '@themost/common';
import LocalScopeAccessConfiguration from './config/scope.access.json'
const el = require('./locales/el.json');
const en = require('./locales/en.json');
export class EventService extends ApplicationService {
    /**
     * @param {IApplication} app
     */
    constructor(app) {
        super(app);
        this.install();
        // extend application service router

        if (app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    // add extra scope access elements
                    const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
                    if (scopeAccess != null) {
                        scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
                    }

                    // add translations
                    const translateService = app.getService(function TranslateService() {
                    });
                    translateService.setTranslation('el', el);
                    translateService.setTranslation('en', en);
                }
            });
        }

    }

    install() {
        // place your code here

    }

    // noinspection JSUnusedGlobalSymbols
    uninstall() {
        // place your code here
    }

}
